package com.maks1m.zombiebird;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.maks1m.zombiebird.screens.GameScreen;
import com.maks1m.zombiebird.screens.SplashScreen;
import com.maks1m.zombiebird.zbhelpers.AssetLoader;

public class ZBGame extends Game {
    @Override
    public void create() {
        Gdx.app.log("ZBGame", "created");

        AssetLoader.load();

//        setScreen(new GameScreen());
        setScreen(new SplashScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }
}
