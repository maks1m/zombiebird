package com.maks1m.zombiebird.TweenAccessors;

public class Value {
    private float val = 1;

    public float getValue() {
        return val;
    }

    public void setValue(float newVal) {
        val = newVal;
    }
}
